<?php

namespace Trilectica\View\Helper;

use \Zend\ServiceManager\ServiceManager;
use \Trilectica\ServiceLocatorAwareInterface\Object as LocatorObject;
use \Zend\Session\Container as SessionContainer;


/**
 * This class is used to manage the notifications created by the system.
 *
 * @owner Trilectica Internet Solutions
 * @author Stefan van de Kaa
 */
class Notifications extends LocatorObject
{

    protected $session = null;

    public function __construct()
    {
        /** @var stdClass $session */
        $session = new SessionContainer('notifications_session');
        if (!isset($session->notifications)) {
            $session->notifications = array();
        }

        $this->session = $session;
    }

    public function addNotification($notification)
    {
        $this->session->notifications[] = $notification;
    }

    public function getNotifications()
    {
        return $this->session->notifications;
    }

    public function removeNotification($key)
    {
        if (isset($this->session->notifications[$key])) {
            unset($this->session->notifications[$key]);
        }
    }
}
