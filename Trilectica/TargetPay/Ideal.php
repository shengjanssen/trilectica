<?php

namespace Trilectica\TargetPay;


class Ideal
{

    protected $sl;

    protected $return;

    protected $trxId;

    /**
     * @param mixed $return
     */
    public function setReturn($return)
    {
        $this->return = $return;
    }

    /**
     * @return mixed
     */
    public function getReturn()
    {
        return $this->return;
    }

    /**
     * @param mixed $sl
     */
    public function setServiceLocator($sl)
    {
        $this->sl = $sl;
    }

    /**
     * @return mixed
     */
    public function getServiceLocator()
    {
        return $this->sl;
    }

    /**
     * @param mixed $trxId
     */
    public function setTrxId($trxId)
    {
        $this->trxId = $trxId;
    }

    /**
     * @return mixed
     */
    public function getTrxId()
    {
        return $this->trxId;
    }

    public function __construct($serviceLocator)
    {
        $this->setServiceLocator($serviceLocator);
    }

    /**
     * Create a new Transaction for Ideal. Returns false if the creation failed.
     * If the creation is successful the url which should be redirected to will be returned.
     *
     * This requires the layout code for the TargetPay account to be placed inside the config file.
     *
     * @param $amountInCents
     * @param $description
     * @param $returnUrl
     * @param $reportUrl
     * @param $bank
     *
     * @return bool
     */
    public function createTransaction($amountInCents, $description, $returnUrl, $reportUrl, $bank)
    {
        $config = $this->getServiceLocator()->get('config');
        $layoutCode = $config['rtlo'];
        $url = "https://www.targetpay.com/ideal/start?"
            . "rtlo=" . $layoutCode
            . "&bank=" . $bank
            . "&description=" . urlencode(substr($description, 0, 32))
            . "&amount=" . $amountInCents
            . "&returnurl=" . urlencode($returnUrl)
            . "&reporturl=" . urlencode($reportUrl);

        $response = file_get_contents($url);
        $arrayResponse = explode('|', $response);

        if (!isset($arrayResponse[1])) {
            $this->setReturn($arrayResponse[0]);
            return false;
        }

        $responseType = explode(' ', $arrayResponse[0]);
        $trxId = $responseType[1];
        $this->setTrxId($trxId);

        if ($responseType[0] === '000000') {
            return $arrayResponse[1];
        }

        $this->setReturn($arrayResponse[0]);
        return false;
    }

    /**
     * Check if the transaction is successful. If so we return true otherwise false.
     *
     * @param $transactionId
     * @param int $test
     * @return bool
     */
    public function checkTransaction($transactionId, $test = 0)
    {
        $config = $this->getServiceLocator()->get('config');
        $layoutCode = $config['rtlo'];

        $once = 0;
        $url = "https://www.targetpay.com/ideal/check?"
            . "rtlo=" . $layoutCode
            . "&trxid=" . $transactionId
            . "&once=" . $once
            . "&test=" . $test;

        $response = file_get_contents($url);
        if ($response == '000000 OK') {
            return true;
        }

        $this->setReturn($response);

        return false;
    }

    /**
     * Check if the request came from a TargetPay server. If so check if the transaction was
     * successful. If successful we return the transaction ID so we can update the record. Otherwise
     * false will be returned
     *
     * @param array $params
     * @return bool
     */
    public function handleTransaction($params = array())
    {
        if (substr($_SERVER['REMOTE_ADDR'], 0, 10) == "89.184.168" || substr(
                $_SERVER['REMOTE_ADDR'],
                0,
                10
            ) == "78.152.58"
        ) {
            if ($params['status'] == '000000 OK') {
                return $params['trxid'];
            }
        }

        return false;
    }

    public function getBanks()
    {
        $xml = file_get_contents('https://www.targetpay.com/ideal/getissuers.php?format=xml ');
        //die($xml);
        $simpleXml = simplexml_load_string($xml);
        $banks = array();
        foreach ($simpleXml as $bank) {
            $banks[(string)$bank->attributes()->id] = (string)$bank;
        }

        return $banks;
    }
}