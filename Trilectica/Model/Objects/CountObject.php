<?php

namespace Trilectica\Model\Objects;

use \Trilectica\Model\Objects\DefaultObject;

class CountObject extends DefaultObject
{

    protected $count = null;

    /**
     * Set the count property
     *
     * @param string $count
     * @return string
     */
    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * Retrieve the count property
     *
     * @return string|null
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set the dbAdapterName property
     *
     * @param string $dbadaptername
     * @return string
     */
    public function setDbadaptername($dbadaptername)
    {
        $this->dbAdapterName = $dbadaptername;
        return $this;
    }

    /**
     * Retrieve the dbAdapterName property
     *
     * @return string|null
     */
    public function getDbadaptername()
    {
        return $this->dbAdapterName;
    }

    /**
     * Exchange the data to the object.
     *
     * @param array $data
     * @return Object
     */
    public function exchangeArray($data)
    {
        $this->count = (isset($data['count'])) ? $data['count'] : null;

        return $this;
    }

    /**
     * Get an array copy of the values in the object.
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return array(
            "count" => $this->count,
        );
    }
}
