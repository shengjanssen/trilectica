<?php

namespace Trilectica\Model\Objects;

use Trilectica\Toolkit\Text;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceManagerAwareInterface;

class DefaultObject implements ServiceManagerAwareInterface
{

    protected $fks = array();

    public function getForeignKeyFields()
    {
        return $this->fks;
    }

    protected $serviceManager;

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;

        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    public function __construct($sm = null)
    {
        $this->serviceManager = $sm;
    }

    public function save()
    {

        if (!method_exists($this, 'getArrayCopy') || !method_exists($this, 'getId')) {
            throw new \Exception('Save cannot be called from outside the object.');
        }

        $className = get_class($this);
        $exploded = explode('\\', $className);
        $className = end($exploded);
        $manager = $this->serviceManager->get($className . "Manager");
        $tableGateway = $manager->getTableGateway();
        $data = $this->getArrayCopy();
        $id = (int)$this->getId();

        if ($id == 0) {
            if (array_key_exists('created_on', $data) && !$data['created_on']) {
                $time = date('Y-m-d H:i:s');
                $data['created_on'] = $time;
                $this->setCreatedOn($time);
            }

            if (array_key_exists('updated_on', $data)) {
                $time = date('Y-m-d H:i:s');
                $data['updated_on'] = $time;
                $this->setUpdatedOn($time);
            }

            if (array_key_exists('seo_url', $data) && array_key_exists('title', $data) && !$data['seo_url']) {
                $text = new Text();
                $slug = $text->stripTitle($data['title']);
                while (count($manager->fetchAll(array('seo_url = ?' => $slug))) > 0) {
                    $slug .= rand(1, 9);
                }
                $data['seo_url'] = $slug;
                $this->setSeoUrl($slug);
            }

            $tableGateway->insert($data);

            if (method_exists($this, 'setId')) {
                $this->setId($tableGateway->getLastInsertValue());
            }
        } else {
            if ($manager->find($id)) {
                if (array_key_exists('updated_on', $data)) {
                    $time = date('Y-m-d H:i:s');
                    $data['updated_on'] = $time;
                    $this->setUpdatedOn($time);
                }

                if (array_key_exists('created_on', $data)) {
                    unset($data['created_on']);
                }

                $tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Record id does not exist');
            }
        }
    }

    public function delete()
    {

        if (!method_exists($this, 'getArrayCopy') || !method_exists($this, 'getId')) {
            throw new \Exception('Save cannot be called from outside the object.');
        }

        $className = get_class($this);
        $exploded = explode('\\', $className);
        $className = end($exploded);
        $manager = $this->serviceManager->get($className . "Manager");
        $tableGateway = $manager->getTableGateway();
        $id = (int)$this->getId();

        $tableGateway->delete(array('id' => $id));
    }

    public function getTitle()
    {
        if (!method_exists($this, 'getArrayCopy') || !method_exists($this, 'getId')) {
            throw new \Exception('Save cannot be called from outside the object.');
        }

        $objectArr = $this->getArrayCopy();
        foreach ($objectArr as $index => $value) {
            // only return if not an ID field.
            if (substr($index, -2) != 'id') {
                return $value;
            }
        }
    }

}