<?php

namespace Trilectica\Model\Renderers;

use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceManagerAwareInterface;

abstract class DefaultRenderer implements ServiceManagerAwareInterface
{
    abstract public function render(\Zend\Form\Element $element);

    protected $serviceManager;

    protected $manager;

    protected $function;

    protected $idField;

    protected $titleField;

    /**
     * @param mixed $idField
     */
    public function setIdField($idField)
    {
        $this->idField = $idField;
    }

    /**
     * @return mixed
     */
    public function getIdField()
    {
        return $this->idField;
    }

    /**
     * @param mixed $titleField
     */
    public function setTitleField($titleField)
    {
        $this->titleField = $titleField;
    }

    /**
     * @return mixed
     */
    public function getTitleField()
    {
        return $this->titleField;
    }

    /**
     * @param mixed $function
     */
    public function setFunction($function)
    {
        $this->function = $function;
    }

    /**
     * @return mixed
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * @param mixed $manager
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return mixed
     */
    public function getManager()
    {
        return $this->manager;
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;

        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    public function __construct($sm = null)
    {
        $this->serviceManager = $sm;
    }
}