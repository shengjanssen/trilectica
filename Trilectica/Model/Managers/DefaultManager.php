<?php

namespace Trilectica\Model\Managers;

use \Zend\Db\TableGateway\TableGateway;

class DefaultManager
{
    protected $tableGateway = null;

    public function setTableGateway(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        return $this;
    }

    public function getTableGateway()
    {
        return $this->tableGateway;
    }

    /**
     * Constructor for the manager.
     *
     * @param TableGateway $tableGateway
     */
    public function __construct($tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
}