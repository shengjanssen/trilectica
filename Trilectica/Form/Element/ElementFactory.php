<?php

namespace Trilectica\Form\Element;

use Trilectica\Form\Element;
use \Zend\Form\Element as ZendElement;

class ElementFactory extends Element\Factory
{

    protected $sl;

    public function __construct($sl)
    {
        $this->sl = $sl;
    }

    public function render(ZendElement $element)
    {

        $translator = $this->sl->get('translator');
        if ($element->getLabel()) {
            $element->setLabel($translator->translate($element->getLabel()));
        }

        if ($element->getMessages()) {
            $messages = $element->getMessages();
            $messagesTranslated = array();
            foreach ($messages as $key => $message) {
                $messagesTranslated[$key] = $translator->translate($message);
            }
            $element->setMessages($messagesTranslated);
        }

        switch (get_class($element)) {
            case 'Zend\Form\Element\Text' :
                $class = new Element\Text();
                break;
            case 'Zend\Form\Element\Email' :
                $class = new Element\Email();
                break;
            case 'Zend\Form\Element\Submit' :
                $class = new Element\Submit();
                break;
            case 'Zend\Form\Element\Select' :
                $class = new Element\Select();
                break;
            case 'Zend\Form\Element\DateTime' :
                $class = new Element\DateTime();
                break;
            case 'Zend\Form\Element\Date' :
                $class = new Element\Date();
                break;
            case 'Zend\Form\Element\Textarea' :
                $class = new Element\TextArea();
                break;
            case 'Zend\Form\Element\Number' :
                $class = new Element\Number($this->sl);
                break;
            case 'Zend\Form\Element\Checkbox' :
                $class = new Element\Checkbox();
                break;
            case 'Zend\Form\Element\Password' :
                $class = new Element\Password();
                break;
            default :
                return 'No renderer for element: ' . get_class($element);
        }

        return $class->render($element);
    }
}