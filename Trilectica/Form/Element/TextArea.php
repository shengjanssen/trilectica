<?php

namespace Trilectica\Form\Element;

use \Zend\Form\Element;
use \Zend\Form\View\Helper;

class TextArea
{
    public function render(Element $element)
    {
        $formLabel = new Helper\FormLabel();
        $formInput = new Helper\FormTextarea();

        if ($element->getOption('ckeditor') !== false) {
            $element->setAttribute('class', $element->getAttribute('class') . ' ckeditor');
        }

        $return = $formLabel($element);
        $return .= '<br style="clear: both;" />';
        $return .= $formInput($element);
        $return .= '<br style="clear: both;" />';
        return $return;
    }
}