<?php

namespace Trilectica\Form\Element;

use \Zend\Form\Element;
use \Zend\Form\View\Helper;

class Text
{
    public function render(Element $element)
    {
        $formLabel = new Helper\FormLabel();
        $formInput = new Helper\FormInput();
        $formErrors = new Helper\FormElementErrors();

        $name = $element->getName();
        if (stripos($name, 'image') !== false
            || stripos($name, 'logo') !== false
            || stripos($name, 'img') !== false
            || (stripos($name, '_url') !== false && $name != 'seo_url')
        ) {
            $element->setAttribute('class', $element->getAttribute('class') . ' kcfinder');
        }
        if (stripos($name, 'keyword') !== false
            || stripos($name, 'tags') !== false
        ) {
            $element->setAttribute('class', $element->getAttribute('class') . ' tags');
        }

        $return = $formLabel($element);
        $return .= $formInput($element);
        $return .= $formErrors($element);
        $return .= '<br style="clear: both;" />';
        return $return;
    }
}