<?php

namespace Trilectica\Form\Element;

use \Zend\Form\Element;
use \Zend\Form\View\Helper\FormInput;

class Submit
{
    public function render(Element\Submit $element)
    {
        $element->setAttribute('class', 'button2 ' . $element->getAttribute('class'));
        $formInput = new FormInput();
        return $formInput($element);
    }
}