<?php

namespace Trilectica\Form\Element;

use \Zend\Form\Element;
use \Zend\Form\View\Helper;

class Number
{

    protected $sl;

    public function __construct($sl = null)
    {
        $this->sl = $sl;
    }

    public function render(Element $element)
    {
        $formLabel = new Helper\FormLabel();
        $formInput = new Helper\FormNumber();

        $renderer = $element->getOption('renderer');
        if ($renderer && $this->sl) {
            if (!class_exists($renderer)) {
                return 'Invalid renderer selected: ' . $renderer;
            }

            $manager = $element->getOption('manager');
            $function = $element->getOption('function');
            $idField = $element->getOption('id-field');
            $titleField = $element->getOption('title-field');

            /** @var $renderer \Trilectica\Model\Renderers\DefaultRenderer */
            $renderer = $this->sl->get($renderer);
            $renderer->setManager($manager);
            $renderer->setFunction($function);
            $renderer->setTitleField($titleField);
            $renderer->setIdField($idField);

            return $renderer->render($element);
        }

        $return = $formLabel($element);
        $return .= $formInput($element);
        $return .= '<br style="clear: both;" />';
        return $return;
    }
}