<?php

namespace Trilectica\Form\Element;

use \Zend\Form\Element;
use \Zend\Form\View\Helper;

class Email
{
    public function render(Element $element)
    {
        $formLabel = new Helper\FormLabel();
        $formInput = new Helper\FormEmail();

        $return = $formLabel($element);
        $return .= $formInput($element);
        $return .= '<br style="clear: both;" />';
        return $return;
    }
}