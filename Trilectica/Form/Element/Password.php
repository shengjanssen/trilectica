<?php

namespace Trilectica\Form\Element;

use \Zend\Form\Element;
use \Zend\Form\View\Helper;

class Password
{
    public function render(Element $element)
    {
        $formLabel = new Helper\FormLabel();
        $formInput = new Helper\FormPassword();
        $formErrors = new Helper\FormElementErrors();

        $return = $formLabel($element);
        $return .= $formInput($element);
        $return .= $formErrors($element);
        $return .= '<br style="clear: both;" />';
        return $return;
    }
}