<?php

namespace Trilectica\Form\Element;

use \Zend\Form\Element;

abstract class Factory
{
    abstract function render(Element $element);
}