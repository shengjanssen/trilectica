<?php

namespace Trilectica\Form;

/**
 * Class to render a ZF2 form. Rendering will use the ZF2 form helpers to achieve
 * the goal.
 *
 * It is meant to render the fields based on the classes name, but this is yet to be
 * implemented.
 *
 * @todo Implement form rendering based on the elements class.
 */
class Renderer extends \Trilectica\ServiceLocatorAwareInterface\Object
{

    protected $form;

    /**
     * Set the form parameter
     *
     * @param \Zend\Form\Form $form
     */
    public function setForm(\Zend\Form\Form $form)
    {
        $this->form = $form;
    }

    /**
     * Render the previously set form and return it as string.
     *
     * @throws \LogicException When the form trying to render has not been set.
     *
     * @return string
     */
    public function render()
    {

        if (!$this->form) {
            throw new \LogicException('Tried to render a form which has not been set yet.', '500');
        }

        /** @var \Zend\Form\Form $form */
        $form = $this->form;
        $form->prepare();

        /** @var $formHelper \Zend\Form\View\Helper\Form */
        $formHelper = $this->sm->get('viewhelpermanager')->get('form');

        /** @var $formHelper \Zend\Form\View\Helper\FormCollection */
        $formCollection = $this->sm->get('viewhelpermanager')->get('formCollection');

        $output = $formHelper->openTag($form);
        $elementFactory = new \Trilectica\Form\Element\ElementFactory($this->getServiceLocator());
        $output .= '<ol class="ui-sortable">';
        foreach ($form->getElements() as $element) {
            $output .= '<li>' . $elementFactory->render($element) . '</li>';
        }
        $output .= '</ol>' . $formHelper->closeTag();

        return $output;
    }
}