<?php

namespace Trilectica\Image;

class Resizer
{
    /*
     * Aspect ratio resize an image,
     * Then center and cutoff excess on the longest part if needed.
     */
    public function adaptiveResize($path, $maxWidth, $maxHeight, $newFileName = null, $quality = 80, $newPath = null)
    {
        $pathInfo = pathinfo($path);
        $ext = $pathInfo['extension'];

        if (!$newFileName) {
            $newFileName = md5(time() . rand()) . "." . $ext;
        }

        if (list($width, $height, $type, $attr) = getimagesize($path)) {
            if ($maxWidth >= $maxHeight) {
                $targetHeight = $height * ($maxWidth / $width);
                $targetWidth = $maxWidth;

                if ($targetHeight >= $maxHeight) {
                    $targetWidth = round($targetWidth / ($targetHeight / $maxHeight));
                    $targetHeight = $maxHeight;
                }
            } else {
                $targetHeight = $maxHeight;
                $targetWidth = $width * ($maxHeight / $height);

                if ($targetWidth >= $maxWidth) {
                    $targetHeight = round($targetHeight / ($targetWidth / $maxWidth));
                    $targetWidth = $maxWidth;
                }
            }
        }

        $imageResource = $this->imageCreateFromFile($path);
        $destinationResource = ImageCreateTrueColor($maxWidth, $maxHeight);

        $iDestinationX = ($maxWidth - $targetWidth != 0) ? (($maxWidth - $targetWidth) / 2) : 0;
        $iDestinationY = ($maxHeight - $targetHeight != 0) ? (($maxHeight - $targetHeight) / 2) : 0;

        imagecopyresampled(
            $destinationResource,
            $imageResource,
            $iDestinationX,
            $iDestinationY,
            0,
            0,
            $targetWidth,
            $targetHeight,
            $width,
            $height
        );
        if ($newPath) {
            if (!file_exists($newPath)) {
                mkdir($newPath, 0777, true);
            }
            imagejpeg($destinationResource, $newPath . '/' . $newFileName, $quality);
        } else {
            if (!file_exists($pathInfo['dirname'])) {
                mkdir($pathInfo['dirname'], 0777, true);
            }
            imagejpeg($destinationResource, $pathInfo['dirname'] . '/' . $newFileName, $quality);
        }

        return $newFileName;

    }

    public function imageCreateFromFile($path)
    {
        $info = @getimagesize($path);

        if (!$info) {
            return false;
        }

        $functions = array(
            IMAGETYPE_GIF => 'imagecreatefromgif',
            IMAGETYPE_JPEG => 'imagecreatefromjpeg',
            IMAGETYPE_PNG => 'imagecreatefrompng',
            IMAGETYPE_WBMP => 'imagecreatefromwbmp',
            IMAGETYPE_XBM => 'imagecreatefromwxbm',
        );

        if (!$functions[$info[2]]) {
            return false;
        }

        if (!function_exists($functions[$info[2]])) {
            return false;
        }

        return $functions[$info[2]]($path);
    }
}