<?php

namespace Trilectica\Acl;

use \Zend\Mvc\MvcEvent;
use \Zend\ServiceManager\ServiceManager;

/**
 * This service is used to validate if the authenticated user has access
 * to the route he is trying to access.
 *
 * The authenticated user will be fetched from the AuthService.
 *
 * @owner Trilectica Internet Solutions
 * @author Stefan van de Kaa
 */
class Service extends \Trilectica\ServiceLocatorAwareInterface\Object
{

    /** @var \Zend\Permissions\Acl\Acl */
    protected $acl;

    /**
     * Set up the ACL roles for user validation.
     */
    public function setupAcl()
    {
        $roles = $this->getServiceLocator()->get('AclRoles');
        $acl = new \Zend\Permissions\Acl\Acl();

        $parents = array();
        foreach ($roles as $roleName => $resources) {

            $role = new \Zend\Permissions\Acl\Role\GenericRole($roleName);
            $acl->addRole($role, $parents);
            $parents[] = $roleName;

            //adding resources
            foreach ($resources as $resource) {
                if (!$acl->hasResource($resource)) {
                    $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
                }
            }

            //adding restrictions
            foreach ($resources as $resource) {
                $acl->allow($role, $resource);
            }
        }
        $this->acl = $acl;
    }

    /**
     * Check if the authenticated user has the rights to view these pages.
     *
     * @param \Zend\Mvc\MvcEvent $e
     */
    public function checkAcl(MvcEvent $e)
    {
        if (!$this->acl) {
            $this->setupAcl();
        }
        $route = $e->getRouteMatch()->getMatchedRouteName();
        /** @var $auth \Trilectica\Auth\Service */
        $auth = $e->getApplication()->getServiceManager()->get('AuthService');
        $user = $auth->getAuthenticatedUser();
        if (!$user) {
            $response = $e->getResponse();

            //location to page or what ever
            $helper = new \Zend\View\Helper\Url();
            $helper->setRouter($e->getRouter());
            $response->getHeaders()->addHeaderLine('Location', $helper('admin-logout'));
            $userRole = 'guest';
        } else {
            $userRole = $user->getRole();
        }

        // if route not in the ACL or no access, redirect to 404 page!
        if (!$this->acl->hasResource($route) || !$this->acl->isAllowed($userRole, $route)) {
            $response = $e->getResponse();

            //location to page or what ever
            $response->getHeaders()->addHeaderLine('Location', $e->getRequest()->getBaseUrl() . '/404');
            $response->setStatusCode(303);
        }
    }

    /**
     * @param $role
     * @param $inherits
     *
     * @return bool
     */
    public function inheritsRole($role, $inherits)
    {
        if (!$this->acl) {
            $this->setupAcl();
        }

        return $role === $inherits || $this->acl->inheritsRole($role, $inherits);
    }
}