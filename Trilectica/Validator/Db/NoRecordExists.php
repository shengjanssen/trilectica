<?php

namespace Trilectica\Validator\Db;

class NoRecordExists extends \Zend\Validator\Db\NoRecordExists
{
    public function __construct($options)
    {
        parent::__construct($options);
        $this->setAdapter(\Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter());
    }
}