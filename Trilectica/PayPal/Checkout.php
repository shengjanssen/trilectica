<?php

namespace Trilectica\PayPal;


class Checkout
{

    protected $environment = 'live'; // or 'beta-sandbox'

    protected $sl;

    protected $return;

    protected $trxId;

    /**
     * @param mixed $trxId
     */
    public function setTrxId($trxId)
    {
        $this->trxId = $trxId;
    }

    /**
     * @return mixed
     */
    public function getTrxId()
    {
        return $this->trxId;
    }

    /**
     * @param mixed $return
     */
    public function setReturn($return)
    {
        $this->return = $return;
    }

    /**
     * @return mixed
     */
    public function getReturn()
    {
        return $this->return;
    }

    /**
     * @param mixed $sl
     */
    public function setServiceLocator($sl)
    {
        $this->sl = $sl;
    }

    /**
     * @return mixed
     */
    public function getServiceLocator()
    {
        return $this->sl;
    }

    public function __construct($serviceLocator)
    {
        $this->setServiceLocator($serviceLocator);
    }

    /**
     * Send HTTP POST Request
     *
     * @param    string    The API method name
     * @param    string    The POST Message fields in &name=value pair format
     * @return    array    Parsed HTTP Response body
     */
    protected function PPHttpPost($methodName_, $nvpStr_)
    {

        $config = $this->getServiceLocator()->get('config');
        $paypalConfig = $config['paypal'];

        // Set up your API credentials, PayPal end point, and API version.
        $API_UserName = urlencode($paypalConfig['username']);
        $API_Password = urlencode($paypalConfig['password']);
        $API_Signature = urlencode($paypalConfig['signature']);

        $API_Endpoint = "https://api-3t.paypal.com/nvp";
        if ("sandbox" === $this->environment || "beta-sandbox" === $this->environment) {
            $API_Endpoint = "https://api-3t.$this->environment.paypal.com/nvp";
        }
        $version = urlencode('51.0');

        // Set the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);

        // Turn off the server and peer verification (TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        // Set the API operation, version, and API signature in the request.
        $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

        // Set the request as a POST FIELD for curl.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

        // Get response from the server.
        $httpResponse = curl_exec($ch);

        if (!$httpResponse) {
            exit("$methodName_ failed: " . curl_error($ch) . '(' . curl_errno($ch) . ')');
        }

        // Extract the response details.
        $httpResponseAr = explode("&", $httpResponse);

        $httpParsedResponseAr = array();
        foreach ($httpResponseAr as $i => $value) {
            $tmpAr = explode("=", $value);
            if (sizeof($tmpAr) > 1) {
                $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
            }
        }

        if ((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
            exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
        }

        return $httpParsedResponseAr;
    }

    public function createTransaction($amountInCents, $description, $itemDescription, $returnUrl, $cancelUrl)
    {

        // Set request-specific fields.
        $paymentAmount = urlencode(number_format($amountInCents / 100, 2));
        $currencyID = urlencode('EUR');
        $paymentType = urlencode('Sale');
        $intTrxId = time();

        $returnURL = urlencode($returnUrl . "/?paypaltrxid=" . $intTrxId . '&paypal=success&amount=' . $paymentAmount);
        $cancelURL = urlencode($cancelUrl . "/?paypal=cancel");
        $description = urlencode($description);

        // Add request-specific fields to the request string.
        $counter = 1;
        $nvpStr = "&Amt=$paymentAmount&ReturnUrl=$returnURL&CANCELURL=$cancelURL&PAYMENTACTION=$paymentType&CURRENCYCODE=$currencyID&L_NAME0=$description&L_DESC0=$itemDescription&e&L_NUMBER0=01234&L_AMT0=$paymentAmount&L_QTY0=$counter&ITEMAMT=$paymentAmount";

        // Execute the API operation; see the PPHttpPost function above.
        $httpParsedResponseAr = $this->PPHttpPost('SetExpressCheckout', $nvpStr);

        if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper(
                $httpParsedResponseAr["ACK"]
            )
        ) {
            // Redirect to paypal.com.
            $token = urldecode($httpParsedResponseAr["TOKEN"]);
            //$intTrxId = $token;
            $strBankURL = "https://www.paypal.com/webscr&cmd=_express-checkout&token=$token&useraction=commit";
            if ("sandbox" === $this->environment || "beta-sandbox" === $this->environment) {
                $strBankURL = "https://www.$this->environment.paypal.com/webscr&cmd=_express-checkout&token=$token&useraction=commit";
            }

            $this->setTrxId($intTrxId);
            return $strBankURL;

        } else {
            $this->setReturn('SetExpressCheckout failed: ' . print_r($httpParsedResponseAr, true));
            return false;
        }

    }

    public function checkTransaction()
    {
        $token = urldecode($_REQUEST["token"]);
        $amount = urldecode($_REQUEST["amount"]);
        $payerId = urldecode($_REQUEST["PayerID"]);
        $nvpStr = "&PayerID=" . urlencode(
                $payerId
            ) . "&TOKEN=" . $token . "&PAYMENTACTION=Sale&Amt=" . $amount . "&CURRENCYCODE=EUR";

        // Execute the API operation; see the PPHttpPost function above.
        $httpParsedResponseAr = $this->PPHttpPost('DoExpressCheckoutPayment', $nvpStr);

        if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper(
                $httpParsedResponseAr["ACK"]
            )
        ) {
            return $_REQUEST['paypaltrxid'];
        }

        return false;
    }
}