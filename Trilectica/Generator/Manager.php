<?php

namespace Trilectica\Generator;

use Zend\Code\Generator\ClassGenerator;
use Zend\Code\Generator\DocBlockGenerator;
use Zend\Code\Generator\DocBlock\Tag;
use Zend\Code\Generator\DocBlock\Tag\GenericTag;
use Zend\Code\Generator\MethodGenerator;
use Zend\Code\Generator\ParameterGenerator;
use Zend\Code\Generator\PropertyGenerator;

use Trilectica\Toolkit\Db;

/**
 * This class is used to generate the manager classes which are used throughout the project.
 * It generates base classes and object classes.
 *
 * @owner Trilectica Internet Solutions
 * @author Stefan van de Kaa
 */
class Manager
{

    /** @var string The output returned for the build. */
    protected $return = '';

    /**
     * @param $tableName
     * @param $objectName
     * @param $tableInfo
     *
     * @return string The output returned for the build.
     */
    public function build($tableName, $objectName, $tableInfo)
    {
        $this->return = '';
        $this->buildBase($tableName, $objectName, $tableInfo);
        $this->buildClass($tableName, $objectName);

        return $this->return;
    }

    /**
     * Build the manager class. This class extends the base which will be generated again and overwritten.
     * Regenerating the model will not override changes made to this file.
     *
     * @param $tableName
     * @param $objectName
     */
    protected function buildClass($tableName, $objectName)
    {

        if (file_exists(__DIR__ . '/../../../../../model/Managers/' . ucfirst($objectName) . 'Manager.php')) {
            $this->return .= "Bestaat reeds<br />";
            return;
        }

        $foo = new ClassGenerator();
        $docblock = DocBlockGenerator::fromArray(
            array(
                'shortDescription' => 'Manager for table ' . $tableName,
                'longDescription' => null,
                'tags' => array(
                    array(
                        'name' => 'owner',
                        'description' => 'Trilectica Internet Solutions',
                    ),
                ),
            )
        );

        $foo->setName($objectName . 'Manager')
            ->setNamespaceName('Model\Managers')
            ->addUse("Model\\Managers\\Base\\" . $objectName . 'Manager', 'Base')
            ->setExtendedClass('Base')
            ->setDocblock($docblock);

        $class = $foo->generate();
        file_put_contents(
            __DIR__ . '/../../../../../model/Managers/' . ucfirst($objectName) . 'Manager.php',
            "<?php\n\n" . $class
        );
        $this->return .= "Class aangemaakt<br />";
    }

    /**
     * Generating the managers base. This file will be overwritten every time the model is
     * generated again.
     *
     * @param $tableName
     * @param $objectName
     * @param $tableInfo
     */
    protected function buildBase($tableName, $objectName, $tableInfo)
    {

        $foo = new ClassGenerator();
        $docblock = DocBlockGenerator::fromArray(
            array(
                'shortDescription' => 'Generated manager class for table ' . $tableName,
                'longDescription' => 'This is a class generated with Zend\Code\Generator.',
                'tags' => array(
                    array(
                        'name' => 'owner',
                        'description' => 'Trilectica Internet Solutions',
                    ),
                ),
            )
        );

        $properties = array();
        $functions = array();
        $functions[] = $this->addFetchAll();
        $functions[] = $this->addCount();
        $functions[] = $this->addFind($objectName);
        $functions[] = $this->addDelete();

        foreach ($tableInfo->getTableNames() as $tableNameConstraint) {
            foreach ($tableInfo->getConstraints($tableNameConstraint) as $constraint) {
                $functions[] = $this->addForeignKeys($constraint, $objectName);
                $functions[] = $this->addForeignKeyCounts($constraint, $objectName);
            }
        }

        $foo->setName($objectName . 'Manager')
            ->setNamespaceName('Model\Managers\Base')
            ->setDocblock($docblock)
            ->addUse('Trilectica\\Model\\Managers\\DefaultManager')
            ->addUse('Zend\\Db\\ResultSet\\ResultSet')
            ->addUse("Zend\\Db\\Sql")
            ->addUse("Zend\\Db\\Sql\\Select")
            ->setExtendedClass('DefaultManager')
            ->addProperties($properties)
            ->addMethods(
            // Method passed as array
                $functions
            );

        $base = $foo->generate();
        $this->return .= "Building base<br />";
        file_put_contents(
            __DIR__ . '/../../../../../model/Managers/Base/' . ucfirst($objectName) . 'Manager.php',
            "<?php\n\n" . $base
        );
    }

    /**
     * Add a fetchAll function to the base class.
     *
     * @return \Zend\Code\Generator\MethodGenerator
     */
    protected function addFetchAll()
    {

        $body = '$resultSet = $this->tableGateway->select(function(Select $select) use ($filters, $order, $limit, $offset) {' . "\n";
        $body .= '    $select->where($filters);' . "\n";
        $body .= '    if ($order) {' . "\n";
        $body .= '        $select->order($order);' . "\n";
        $body .= '    }' . "\n";
        $body .= '    if ($limit) {' . "\n";
        $body .= '        $select->limit($limit);' . "\n";
        $body .= '    }' . "\n";
        $body .= '    if ($offset) {' . "\n";
        $body .= '        $select->offset($offset);' . "\n";
        $body .= '    }' . "\n";
        $body .= "});\n\n";
        $body .= 'return $resultSet;';

        return MethodGenerator::fromArray(
            array(
                'name' => 'fetchAll',
                'parameters' => array(
                    ParameterGenerator::fromArray(array('name' => 'filters', 'defaultvalue' => array())),
                    ParameterGenerator::fromArray(array('name' => 'order', 'defaultvalue' => null)),
                    ParameterGenerator::fromArray(array('name' => 'limit', 'defaultvalue' => null)),
                    ParameterGenerator::fromArray(array('name' => 'offset', 'defaultvalue' => null))
                ),
                'body' => $body,
                'docblock' => DocBlockGenerator::fromArray(
                        array(
                            'shortDescription' => 'Fetch all records.',
                            'longDescription' => null,
                            'tags' => array(
                                new Tag\ParamTag('filters', 'array'),
                                new Tag\ParamTag('order', 'string|array'),
                                new Tag\ParamTag('limit', 'integer'),
                                new Tag\ParamTag('offset', 'integer'),
                                new Tag\ReturnTag(array(
                                    'datatype' => '\\Zend\\Db\\ResultSet\\ResultSet|null',
                                ))
                            )
                        )
                    ),
            )
        );
    }

    /**
     * Add a fetchAll function to the base class.
     *
     * @return \Zend\Code\Generator\MethodGenerator
     */
    protected function addCount()
    {

        $body = '$resultSet = $this->tableGateway->select(' . "\n";
        $body .= '    function(Sql\Select $select) use ($filters) {' . "\n";
        $body .= '        $select->columns(array("count" => new Sql\Expression("COUNT(*)")))' . "\n";
        $body .= '            ->where($filters);' . "\n";
        $body .= '    }' . "\n";
        $body .= ');' . "\n";


        $tags[] = new Tag\ReturnTag(array(
            'datatype' => '\\Zend\\Db\\ResultSet\\ResultSet',
        ));

        $body .= '$resultSet->setArrayObjectPrototype(new \Trilectica\Model\Objects\CountObject());' . "\n";
        $body .= 'return $resultSet->current()->getCount();' . "\n";

        return MethodGenerator::fromArray(
            array(
                'name' => 'count',
                'parameters' => array(
                    ParameterGenerator::fromArray(array('name' => 'filters', 'defaultvalue' => array()))
                ),
                'body' => $body,
                'docblock' => DocBlockGenerator::fromArray(
                        array(
                            'shortDescription' => 'Count all records.',
                            'longDescription' => null,
                            'tags' => array(
                                new Tag\ParamTag('filters', 'array'),
                                new Tag\ReturnTag(array(
                                    'datatype' => 'integer',
                                ))
                            )
                        )
                    ),
            )
        );
    }

    /**
     * Add a find method to the base class.
     *
     * @param $objectName
     * @return \Zend\Code\Generator\MethodGenerator
     */
    protected function addFind($objectName)
    {

        $body = '$id  = (int) $id;' . "\n\n";
        $body .= '$resultSet = $this->tableGateway->select(' . "\n";
        $body .= '    array("id" => $id)' . "\n";
        $body .= ');' . "\n\n";
        $body .= 'return $resultSet->current();' . "\n";

        return MethodGenerator::fromArray(
            array(
                'name' => 'find',
                'parameters' => array('id'),
                'body' => $body,
                'docblock' => DocBlockGenerator::fromArray(
                        array(
                            'shortDescription' => 'Get by the ID',
                            'longDescription' => 'Returns null when not found',
                            'tags' => array(
                                new Tag\ParamTag('id', 'int'),
                                new Tag\ReturnTag(array(
                                    'datatype' => '\\Model\\Objects\\' . $objectName,
                                )),
                            ),
                        )
                    ),
            )
        );
    }

    /**
     * Add a delete method to the base class.
     *
     * @return \Zend\Code\Generator\MethodGenerator
     */
    protected function addDelete()
    {

        $body = '$id  = (int) $id;' . "\n\n";
        $body .= 'return $this->tableGateway->delete(' . "\n";
        $body .= '    array("id" => $id)' . "\n";
        $body .= ');';

        return MethodGenerator::fromArray(
            array(
                'name' => 'delete',
                'parameters' => array('id'),
                'body' => $body,
                'docblock' => DocBlockGenerator::fromArray(
                        array(
                            'shortDescription' => 'Delete by the ID',
                            'longDescription' => null,
                            'tags' => array(
                                new Tag\ParamTag('id', 'int'),
                                new Tag\ReturnTag(array(
                                    'datatype' => 'int',
                                )),
                            ),
                        )
                    ),
            )
        );
    }

    /**
     * Add foreign key methods to the base class.
     *
     * @param $constraint
     * @param $objectName
     * @return bool|\Zend\Code\Generator\MethodGenerator
     */
    protected function addForeignKeys($constraint, $objectName)
    {
        /** @var $constraint \Zend\Db\Metadata\Object\ConstraintObject */
        if (!$constraint->hasColumns()) {
            return false;
        }

        if (!$constraint->isForeignKey()) {
            return false;
        }

        $toolkit = new Db();
        $columnsName = $toolkit->tableToObjectName(implode(', ', $constraint->getColumns()));

        if ($toolkit->tableToObjectName($constraint->getTableName()) != $objectName) {
            return;
        }

        $useString = '$filters, $order, $limit, $offset';
        foreach ($constraint->getColumns() as $col) {
            $useString .= ', $' . $col;
        }

        $body = '$resultSet = $this->tableGateway->select(function(Select $select) use (' . $useString . ') {' . "\n";
        $body .= '    $filters = array_merge($filters, array(' . "\n";

        $tags = array();
        foreach ($constraint->getColumns() as $col) {
            $body .= '            "' . $col . '" => $' . $col . ',' . "\n";
            $tags[] = new Tag\ParamTag($col);
        }

        $tags[] = new Tag\ParamTag('filters');
        $tags[] = new Tag\ParamTag('order');
        $tags[] = new Tag\ParamTag('limit');
        $tags[] = new Tag\ParamTag('offset');
        $tags[] = new Tag\ReturnTag(array(
            'datatype' => '\\Zend\\Db\\ResultSet\\ResultSet',
        ));

        $body .= '    ));' . "\n\n";
        $body .= '    $select->where($filters);' . "\n\n";
        $body .= '    if ($order) {' . "\n";
        $body .= '        $select->order($order);' . "\n";
        $body .= '    }' . "\n";
        $body .= '    if ($limit) {' . "\n";
        $body .= '        $select->limit($limit);' . "\n";
        $body .= '    }' . "\n";
        $body .= '    if ($offset) {' . "\n";
        $body .= '        $select->offset($offset);' . "\n";
        $body .= '    }' . "\n";
        $body .= "});\n\n";

        $body .= 'return $resultSet;' . "\n";

        $paramsTags = $constraint->getColumns();
        $paramsTags[] = ParameterGenerator::fromArray(array('name' => 'filters', 'defaultvalue' => array()));
        $paramsTags[] = ParameterGenerator::fromArray(array('name' => 'order', 'defaultvalue' => null));
        $paramsTags[] = ParameterGenerator::fromArray(array('name' => 'limit', 'defaultvalue' => null));
        $paramsTags[] = ParameterGenerator::fromArray(array('name' => 'offset', 'defaultvalue' => null));

        return MethodGenerator::fromArray(
            array(
                'name' => 'fetchBy' . $columnsName,
                'parameters' => $paramsTags,
                'body' => $body,
                'docblock' => DocBlockGenerator::fromArray(
                        array(
                            'shortDescription' => 'Get through foreign key',
                            'longDescription' => null,
                            'tags' => $tags,
                        )
                    ),
            )
        );
    }

    /**
     * Add foreign key count methods to the base class.
     *
     * @param $constraint
     * @param $objectName
     * @return bool|\Zend\Code\Generator\MethodGenerator
     */
    protected function addForeignKeyCounts($constraint, $objectName)
    {
        /** @var $constraint \Zend\Db\Metadata\Object\ConstraintObject */
        if (!$constraint->hasColumns()) {
            return false;
        }

        if (!$constraint->isForeignKey()) {
            return false;
        }

        $toolkit = new Db();
        $columnsName = $toolkit->tableToObjectName(implode(', ', $constraint->getColumns()));

        if ($toolkit->tableToObjectName($constraint->getTableName()) != $objectName) {
            return;
        }

        $body = '$resultSet = $this->tableGateway->select(' . "\n";
        $body .= '    function(Sql\Select $select) use ($filters, ' . "\n";
        $columns = $constraint->getColumns();
        array_walk(
            $columns,
            function (&$item) {
                $item = '$' . $item;
            }
        );
        $body .= '        ' . implode(', ', $columns) . "\n";
        $body .= '    ) {' . "\n";

        $body .= '        $filters = array_merge($filters, array(' . "\n";

        $tags = array();
        foreach ($constraint->getColumns() as $col) {
            $body .= '                "' . $col . '" => $' . $col . ',' . "\n";
            $tags[] = new Tag\ParamTag($col);
        }

        $tags[] = new Tag\ParamTag('filters');
        $tags[] = new Tag\ReturnTag(array(
            'datatype' => '\\Zend\\Db\\ResultSet\\ResultSet',
        ));

        $body .= '        ));' . "\n\n";

        $body .= '        $select->columns(array("count" => new Sql\Expression("COUNT(*)")))' . "\n";
        $body .= '            ->where($filters);' . "\n\n";
        $body .= '    }' . "\n";
        $body .= ');' . "\n";


        $tags[] = new Tag\ReturnTag(array(
            'datatype' => 'integer',
        ));

        $body .= '$resultSet->setArrayObjectPrototype(new \Trilectica\Model\Objects\CountObject());' . "\n";
        $body .= 'return $resultSet->current()->getCount();' . "\n";

        $paramsTags = $constraint->getColumns();
        $paramsTags[] = ParameterGenerator::fromArray(array('name' => 'filters', 'defaultvalue' => array()));

        return MethodGenerator::fromArray(
            array(
                'name' => 'countBy' . $columnsName,
                'parameters' => $paramsTags,
                'body' => $body,
                'docblock' => DocBlockGenerator::fromArray(
                        array(
                            'shortDescription' => 'Get count through foreign key',
                            'longDescription' => null,
                            'tags' => $tags,
                        )
                    ),
            )
        );
    }
}