<?php

namespace Trilectica\Generator;

use Zend\Code\Generator\ClassGenerator;
use Zend\Code\Generator\DocBlockGenerator;
use Zend\Code\Generator\DocBlock\Tag\GenericTag;
use Zend\Code\Generator\DocBlock\Tag;
use Zend\Code\Generator\MethodGenerator;
use Zend\Code\Generator\ParameterGenerator;
use Zend\Code\Generator\PropertyGenerator;

use Trilectica\Toolkit\Db;
use Trilectica\Toolkit\Text;

/**
 * This class is used to generate the object classes which are used throughout the project.
 * It generates base classes and object classes.
 *
 * @owner Trilectica Internet Solutions
 * @author Stefan van de Kaa
 */
class Object
{

    /** @var string The string with data which is returned. */
    protected $return = '';

    /**
     * Build the model!
     *
     * @param $tableName
     * @param $objectName
     * @param $tableInfo
     * @param $adapterName
     *
     * @return string The information about the build.
     */
    public function build($tableName, $objectName, $tableInfo, $adapterName)
    {
        $this->return = '';
        $this->buildBase($tableName, $objectName, $tableInfo, $adapterName);
        $this->buildClass($tableName, $objectName);
        return $this->return;
    }

    /**
     * Build the class which will extend the base. This is the object which will be used throughout
     * the application.
     *
     * If the object which is tried to be build already exists it will be skipped since we don't want to
     * loose any existing code.
     *
     * @param $tableName
     * @param $objectName
     */
    protected function buildClass($tableName, $objectName)
    {

        if (file_exists(__DIR__ . '/../../../../../model/Objects/' . ucfirst($objectName) . '.php')) {
            $this->return .= "Bestaat reeds<br />";
            return;
        }

        $foo = new ClassGenerator();
        $docblock = DocBlockGenerator::fromArray(
            array(
                'shortDescription' => 'Class for table ' . $tableName,
                'longDescription' => null,
                'tags' => array(
                    array(
                        'name' => 'owner',
                        'description' => 'Trilectica Internet Solutions',
                    ),
                ),
            )
        );
        $docblock->setWordWrap(false);

        $foo->setName($objectName)
            ->setNamespaceName('Model\Objects')
            ->addUse("Model\\Objects\\Base\\" . ucfirst($objectName), 'Base')
            ->setExtendedClass('Base')
            ->setDocblock($docblock);

        $class = $foo->generate();
        file_put_contents(
            __DIR__ . '/../../../../../model/Objects/' . ucfirst($objectName) . '.php',
            "<?php\n\n" . $class
        );
        $this->return .= "Class aangemaakt<br />";
    }

    /**
     * Build the base class for the object. Will create annotations for form generation.
     * Depending on the datatype of the column validation will be added.
     *
     * @todo add new datatypes if neccessary for annotations.
     *
     * @param $tableName
     * @param $objectName
     * @param $tableInfo
     * @param $adapterName
     *
     * @throws Exception When datatype not implemented yet
     */
    protected function buildBase($tableName, $objectName, $tableInfo, $adapterName)
    {
        $this->return .= 'Building base<br />';
        $foo = new ClassGenerator();
        $docblock = DocBlockGenerator::fromArray(
            array(
                'shortDescription' => 'Generated class for table ' . $tableName,
                'longDescription' => 'This is a class generated with Zend\Code\Generator.',
                'tags' => array(
                    array(
                        'name' => 'owner',
                        'description' => 'Trilectica Internet Solutions',
                    ),
                    array(
                        'name' => 'Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")',
                        'description' => '',
                    ),
                    array(
                        'name' => 'Annotation\Name("' . $objectName . '")',
                        'description' => '',
                    )
                ),
            )
        );
        $docblock->setWordWrap(false);

        $toolkit = new Db();
        $toolkitText = new \Trilectica\Toolkit\Text;
        $table = $tableInfo->getTable($tableName);
        $properties = array();
        $functions = array();
        $columns = $table->getColumns();

        /** @var $column \Zend\Db\Metadata\Object\ColumnObject */
        foreach ($columns as $column) {
            $columnName = $toolkitText->underscoreToUpper($column->getName());

            $datatype = $column->getDataType();
            switch ($datatype) {
                case 'int' :
                    $isForeignKey = false;
                    // check for foreignKey
                    foreach ($table->getConstraints() as $contraint) {
                        if ($contraint->isForeignKey()) {
                            foreach ($contraint->getColumns() as $col) {
                                if ($col == $column->getName()) {
                                    $properties[] = $this->generateFKProperty($column, $contraint);
                                    $isForeignKey = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (!$isForeignKey) {
                        $properties[] = $this->generateIntProperty($column);
                    }
                    break;
                case 'tinyint' :
                    $properties[] = $this->generateTinyIntProperty($column);
                    break;
                case 'float' :
                    $properties[] = $this->generateFloatProperty($column);
                    break;
                case 'varchar' :
                    if ($column->getName() == 'password') {
                        $properties[] = $this->generatePasswordProperty($column);
                    } else {
                        $properties[] = $this->generateVarcharProperty($column);
                    }
                    break;
                case 'enum' :
                    $properties[] = $this->generateEnumProperty($column);
                    break;
                case 'datetime' :
                case 'timestamp' :
                    $properties[] = $this->generateTimestampProperty($column);
                    break;
                case 'date' :
                    $properties[] = $this->generateDateProperty($column);
                    break;
                case 'text' :
                    $properties[] = $this->generateTextProperty($column);
                    break;
                default:
                    $properties[] = $this->generateVarcharProperty($column);
                    break;
            }
            $functions[] = $this->buildSetter($column->getName());
            $functions[] = $this->buildGetter($column->getName());
        }

        $properties[] = PropertyGenerator::fromArray(
            array(
                'name' => 'dbAdapterName',
                'defaultValue' => $adapterName,
                'flags' => PropertyGenerator::FLAG_PROTECTED
            )
        );
        $functions[] = $this->buildSetter('dbAdapterName');
        $functions[] = $this->buildGetter('dbAdapterName');
        $functions[] = $this->buildExchangeArray($columns);
        $functions[] = $this->buildArrayCopy($columns);

        /*
         * Add foreign key functions to the object
         */
        $foreigners = array();
        foreach ($tableInfo->getConstraints($tableName) as $constraint) {
            $postfix = '';
            if (in_array($constraint->getReferencedTableName(), $foreigners)) {
                $constraintColumns = $constraint->getColumns();
                $postfix = explode('_', $constraintColumns[0]);
                $postfix = ucfirst($postfix[0]);
            }

            $foreigners[] = $constraint->getReferencedTableName();

            $keyInfo = $this->addForeignKey($constraint, $postfix);
            $functions[] = $keyInfo[1];
            $properties[] = $keyInfo[0];
        }

        /**
         * add foreign key functions to the object reversed
         */
        $foreigners = array();
        foreach ($tableInfo->getTableNames() as $tableNameConstraint) {
            foreach ($tableInfo->getConstraints($tableNameConstraint) as $constraint) {
                $postfix = '';
                if (in_array($constraint->getTableName(), $foreigners)) {
                    $constraintColumns = $constraint->getColumns();
                    $postfix = explode('_', $constraintColumns[0]);
                    $postfix = ucfirst($postfix[0]);
                }
                if ($tableName == $constraint->getReferencedTableName() && $constraint->hasColumns(
                    ) && $constraint->isForeignKey() && $toolkit->tableToObjectName(
                        $constraint->getReferencedTableName()
                    ) == $objectName
                ) {
                    $foreigners[] = $constraint->getTableName();
                }

                $keyInfo = $this->addForeignKeyReversed($constraint, $objectName, $postfix);
                $functions[] = $keyInfo[1];
                $properties[] = $keyInfo[0];
                $functions[] = $this->addForeignKeyCountReversed($constraint, $objectName, $postfix);
            }
        }

        $foo->setName($objectName)
            ->setNamespaceName('Model\Objects\Base')
            ->addUse('Trilectica\\Model\\Objects\\DefaultObject')
            ->addUse('\Zend\Form\Annotation')
            ->setExtendedClass('DefaultObject')
            ->setDocblock($docblock)
            ->addProperties($properties)
            ->addMethods($functions);

        $base = $foo->generate();
        file_put_contents(
            __DIR__ . '/../../../../../model/Objects/Base/' . ucfirst($objectName) . '.php',
            "<?php\n\n" . $base
        );
    }

    protected function buildSetter($columnName)
    {
        $toolkit = new \Trilectica\Toolkit\Text();

        $docblock = DocBlockGenerator::fromArray(
            array(
                'shortDescription' => 'Set the ' . $columnName . ' property',
                'longDescription' => null,
                'tags' => array(
                    new Tag\ParamTag($toolkit->underscoreToUpper($columnName), 'string'),
                    new Tag\ReturnTag(array(
                        'datatype' => 'string',
                    )),
                ),
            )
        );
        $docblock->setWordWrap(false);

        return MethodGenerator::fromArray(
            array(
                'name' => 'set' . ucfirst($toolkit->underscoreToUpper($columnName)),
                'parameters' => array($toolkit->underscoreToUpper($columnName)),
                'body' => '$this->' . $columnName . ' = $' . $toolkit->underscoreToUpper(
                        $columnName
                    ) . ';' . "\n" . 'return $this;',
                'docblock' => $docblock
            )
        );
    }

    protected function buildGetter($columnName)
    {
        $toolkit = new \Trilectica\Toolkit\Text();

        $docblock = DocBlockGenerator::fromArray(
            array(
                'shortDescription' => 'Retrieve the ' . $columnName . ' property',
                'longDescription' => null,
                'tags' => array(
                    new Tag\ReturnTag(
                        array(
                            'datatype' => 'string|null',
                        )
                    ),
                ),
            )
        );
        $docblock->setWordWrap(false);

        return new MethodGenerator(
            'get' . ucfirst($toolkit->underscoreToUpper($columnName)),
            array(),
            MethodGenerator::FLAG_PUBLIC,
            'return $this->' . $columnName . ';',
            $docblock
        );
    }

    protected function buildExchangeArray($fields)
    {
        $toolkitText = new Text();
        $body = '';
        foreach ($fields as $field) {
            $body .= 'if (isset($data[\'' . $field->getName() . '\'])) {' . "\n";
            $body .= '    $this->' . $field->getName() . ' = $data[\'' . $field->getName() . '\'];' . "\n";
            $body .= '}' . "\n";
        }

        $docblock = DocBlockGenerator::fromArray(
            array(
                'shortDescription' => 'Exchange the data to the object.',
                'longDescription' => null,
                'tags' => array(
                    new Tag\ParamTag('data', 'array'),
                    new Tag\ReturnTag(array(
                        'datatype' => 'Object',
                    )),
                ),
            )
        );
        $docblock->setWordWrap(false);

        return MethodGenerator::fromArray(
            array(
                'name' => 'exchangeArray',
                'parameters' => array('data'),
                'body' => $body . "\n" . 'return $this;',
                'docblock' => $docblock
            )
        );
    }

    /**
     * Create an arrayCopy function. This function is used for binding the object
     * to a form.
     *
     * @param $fields
     * @return \Zend\Code\Generator\MethodGenerator
     */
    protected function buildArrayCopy($fields)
    {
        $toolkitText = new Text();
        $body = 'return array(' . "\n";
        foreach ($fields as $field) {
            if ($field->getName() === 'updated_on') {
                $body .= '    "' . $field->getName() . '" => null' . ",\n";
            } else {
                $body .= '    "' . $field->getName() . '" => $this->' . $field->getName() . ",\n";
            }
        }
        $body .= ');' . "\n";

        $docblock = DocBlockGenerator::fromArray(
            array(
                'shortDescription' => 'Get an array copy of the values in the object.',
                'longDescription' => null,
                'tags' => array(
                    new Tag\ReturnTag(array(
                        'datatype' => 'array',
                    )),
                ),
            )
        );
        $docblock->setWordWrap(false);

        return MethodGenerator::fromArray(
            array(
                'name' => 'getArrayCopy',
                'parameters' => array(),
                'body' => $body,
                'docblock' => $docblock
            )
        );
    }

    protected function addForeignKey($constraint, $postfix = '')
    {
        /** @var $constraint Zend\Db\Metadata\Object\ConstraintObject */
        if (!$constraint->hasColumns()) {
            return false;
        }

        if (!$constraint->isForeignKey()) {
            return false;
        }

        $toolkit = new Db();
        $toolkitText = new Text();

        $referencedTableName = $constraint->getReferencedTableName();
        $columnsName = $toolkit->tableToObjectName(implode(', ', $constraint->getColumns()));
        $objectName = $toolkit->tableToObjectName($referencedTableName);
        $functionName = $toolkit->tableToObjectName($referencedTableName) . $postfix;
        $variableName = $toolkitText->underscoreToUpper($referencedTableName) . $postfix;

        $body = 'if ($this->' . $variableName . ') {' . "\n";
        $body .= '    return $this->' . $variableName . ";\n";
        $body .= "}\n\n";

        $body .= '$this->' . $variableName . ' = $this->getServiceManager()' . "\n";
        $body .= '    ->get("' . $objectName . 'Manager")' . "\n";
        $body .= '    ->find($this->get' . $columnsName . '());' . "\n";
        $body .= "\n";
        $body .= 'return $this->' . $variableName . ";";

        $docblock = DocBlockGenerator::fromArray(
            array(
                'shortDescription' => 'Get ' . $functionName . ' through foreign key',
                'longDescription' => null,
                'tags' => array(
                    new Tag\ReturnTag(array(
                        'datatype' => '\\Model\\Objects\\' . $objectName . '|null',
                    )),
                ),
            )
        );
        $docblock->setWordWrap(false);

        return array(
            array($variableName, null, PropertyGenerator::FLAG_PROTECTED),
            MethodGenerator::fromArray(
                array(
                    'name' => 'find' . $functionName,
                    'parameters' => array(),
                    'body' => $body,
                    'docblock' => $docblock
                )
            )
        );
    }


    protected function addForeignKeyReversed($constraint, $objectName, $postfix = '')
    {
        /** @var $constraint \Zend\Db\Metadata\Object\ConstraintObject */
        if (!$constraint->hasColumns()) {
            return false;
        }

        if (!$constraint->isForeignKey()) {
            return false;
        }

        $toolkit = new Db();
        $toolkitText = new Text();

        if ($toolkit->tableToObjectName($constraint->getReferencedTableName()) != $objectName) {
            return;
        }

        /*
        $body = 'if ($this->' . $toolkitText->plural($constraint->getTableName()) . $postfix . ') {' . "\n";
        $body .= '    return $this->' . $toolkitText->plural($constraint->getTableName()) . $postfix . ';' . "\n";
        $body .= "}\n\n";
        */

        $body = '$this->' . $toolkitText->plural(
                $constraint->getTableName()
            ) . $postfix . ' = $this->getServiceManager()' . "\n";
        $body .= '    ->get("' . $toolkit->tableToObjectName($constraint->getTableName()) . 'Manager")' . "\n";
        $body .= '    ->fetchBy' . $toolkit->tableToObjectName(
                implode('', $constraint->getColumns())
            ) . '($this->get' . $toolkit->tableToObjectName(
                implode('', $constraint->getReferencedColumns())
            ) . '(), $filters, $order, $limit, $offset);' . "\n\n";
        $body .= 'return $this->' . $toolkitText->plural($constraint->getTableName()) . $postfix . ';';

        $paramsTags = array();
        $paramsTags[] = ParameterGenerator::fromArray(array('name' => 'filters', 'defaultvalue' => array()));
        $paramsTags[] = ParameterGenerator::fromArray(array('name' => 'order', 'defaultvalue' => null));
        $paramsTags[] = ParameterGenerator::fromArray(array('name' => 'limit', 'defaultvalue' => null));
        $paramsTags[] = ParameterGenerator::fromArray(array('name' => 'offset', 'defaultvalue' => null));

        $docblock = DocBlockGenerator::fromArray(
            array(
                'shortDescription' => 'Get through foreign key',
                'longDescription' => null,
                'tags' => array(
                    new Tag\ReturnTag(array(
                        'datatype' => '\\Zend\\Db\\ResultSet\\ResultSet',
                    )),
                ),
            )
        );
        $docblock->setWordWrap(false);

        return array(
            array(
                $toolkitText->plural($constraint->getTableName()) . $postfix,
                null,
                PropertyGenerator::FLAG_PROTECTED
            ),
            MethodGenerator::fromArray(
                array(
                    'name' => 'fetch' . $toolkit->tableToObjectName(
                            $toolkitText->plural($constraint->getTableName())
                        ) . $postfix,
                    'parameters' => $paramsTags,
                    'body' => $body,
                    'docblock' => $docblock
                )
            )
        );
    }


    protected function addForeignKeyCountReversed($constraint, $objectName, $postfix = '')
    {
        /** @var $constraint \Zend\Db\Metadata\Object\ConstraintObject */
        if (!$constraint->hasColumns()) {
            return false;
        }

        if (!$constraint->isForeignKey()) {
            return false;
        }

        $toolkit = new Db();
        $toolkitText = new Text();

        if ($toolkit->tableToObjectName($constraint->getReferencedTableName()) != $objectName) {
            return;
        }

        $body = '$count = $this->getServiceManager()' . "\n";
        $body .= '    ->get("' . $toolkit->tableToObjectName($constraint->getTableName()) . 'Manager")' . "\n";
        $body .= '    ->countBy' . $toolkit->tableToObjectName(
                implode('', $constraint->getColumns())
            ) . '($this->get' . $toolkit->tableToObjectName(
                implode('', $constraint->getReferencedColumns())
            ) . '());' . "\n\n";
        $body .= 'return $count;';

        $docblock = DocBlockGenerator::fromArray(
            array(
                'shortDescription' => 'Get count through foreign key',
                'longDescription' => null,
                'tags' => array(
                    new Tag\ReturnTag(array(
                        'datatype' => 'int',
                    )),
                ),
            )
        );
        $docblock->setWordWrap(false);

        return MethodGenerator::fromArray(
            array(
                'name' => 'count' . $toolkit->tableToObjectName(
                        $toolkitText->plural($constraint->getTableName())
                    ) . $postfix,
                'parameters' => array(),
                'body' => $body,
                'docblock' => $docblock
            )
        );
    }

    /**
     * Generate property and docblock for integer fields.
     *
     * Also generates annotations for the form. Does not generate annotations if
     * the field name is in the skipAnnotationFields array. This so these fields will not show up in a add/edit form.
     *
     * Further foreign keys will be skipped since these will show up in other fields!
     *
     * @param \Zend\Db\Metadata\Object\ColumnObject
     *
     * @return \Zend\Code\Generator\PropertyGenerator
     */
    protected function generateIntProperty(\Zend\Db\Metadata\Object\ColumnObject $column)
    {

        $toolkitText = new \Trilectica\Toolkit\Text();
        $columnName = $column->getName();

        $skipAnnotationFields = array('id');
        $docblock = null;

        if (!in_array($column->getName(), $skipAnnotationFields)) {

            $annotationTags = array();

            $defaultTags = array(
                new GenericTag('description', 'Property ' . $columnName),
                new GenericTag('type', 'int'),
            );


            $annotationTags[] = new GenericTag('Annotation\Type("Zend\Form\Element\Number")', '');
            $annotationTags[] = new GenericTag('Annotation\Required(' . ($column->isNullable(
            ) ? 'false' : 'true') . ')', '');
            $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StripTags"})', '');
            $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StringTrim"})', '');
            if (!$column->isNullable()) {
                $annotationTags[] = new GenericTag('Annotation\Validator({"name":"StringLength", "options":{"min":"1"}})', '');
            }
            $label = ucfirst($columnName);
            $label = str_replace('_', ' ', $label);
            $annotationTags[] = new GenericTag('Annotation\Options({"label":"' . $label . '"})', '');

            $docblock = new DocBlockGenerator(
                null,
                null,
                array_merge($defaultTags, $annotationTags)
            );
            $docblock->setWordWrap(false);
        }

        $params = array(
            'name' => $columnName,
            'defaultValue' => null,
            'flags' => PropertyGenerator::FLAG_PROTECTED,
        );

        if ($docblock) {
            $params['docblock'] = $docblock;
        }
        return PropertyGenerator::fromArray($params);
    }

    /**
     * Generate property and docblock for Foreign Keys
     *
     * Also generates annotations for the form. Does not generate annotations if
     * the field name is in the skipAnnotationFields array. This so these fields will not show up in a add/edit form.
     *
     * @param \Zend\Db\Metadata\Object\ColumnObject
     * @param \Zend\Db\Metadata\Object\ConstraintObject
     *
     * @return \Zend\Code\Generator\PropertyGenerator
     */
    protected function generateFKProperty(
        \Zend\Db\Metadata\Object\ColumnObject $column,
        \Zend\Db\Metadata\Object\ConstraintObject $constraint
    ) {

        $toolkitText = new \Trilectica\Toolkit\Text();
        $columnName = $column->getName();

        $skipAnnotationFields = array('id');
        $docblock = null;

        if (!in_array($column->getName(), $skipAnnotationFields)) {

            $annotationTags = array();

            $defaultTags = array(
                new GenericTag('description', 'Property ' . $columnName),
                new GenericTag('type', 'int'),
            );


            $annotationTags[] = new GenericTag('Annotation\Type("Zend\Form\Element\Number")', '');
            $annotationTags[] = new GenericTag('Annotation\Required(' . ($column->isNullable(
            ) ? 'false' : 'true') . ')', '');
            $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StripTags"})', '');
            $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StringTrim"})', '');
            if (!$column->isNullable()) {
                $annotationTags[] = new GenericTag('Annotation\Validator({"name":"StringLength", "options":{"min":"1"}})', '');
            }
            $label = ucfirst($columnName);
            if (substr($label, -3) == '_id') {
                $label = substr($label, 0, -3);
            }
            $label = str_replace('_', ' ', $label);
            $annotationTags[] = new GenericTag('Annotation\Options({"label":"' . $label . '", "renderer":"\Model\Renderers\ForeignKey", "manager":"' . ucfirst(
                $toolkitText->underscoreToUpper($constraint->getReferencedTableName())
            ) . 'Manager", "function":"fetchAll", "id-field":"Id", "title-field":"title"})', '');

            $docblock = new DocBlockGenerator(
                null,
                null,
                array_merge($defaultTags, $annotationTags)
            );
            $docblock->setWordWrap(false);
        }

        $params = array(
            'name' => $columnName,
            'defaultValue' => null,
            'flags' => PropertyGenerator::FLAG_PROTECTED,
        );

        if ($docblock) {
            $params['docblock'] = $docblock;
        }
        return PropertyGenerator::fromArray($params);
    }

    /**
     * Generate property and docblock for float fields.
     *
     * Also generates annotations for the form. Does not generate annotations if
     * the field name is in the skipAnnotationFields array. This so these fields will not show up in a add/edit form.
     *
     * @param \Zend\Db\Metadata\Object\ColumnObject
     *
     * @return \Zend\Code\Generator\PropertyGenerator
     */
    protected function generateFloatProperty(\Zend\Db\Metadata\Object\ColumnObject $column)
    {

        $toolkitText = new \Trilectica\Toolkit\Text();
        $columnName = $column->getName();

        $docblock = null;

        $annotationTags = array();

        $defaultTags = array(
            new GenericTag('description', 'Property ' . $columnName),
            new GenericTag('type', 'float'),
        );


        $annotationTags[] = new GenericTag('Annotation\Type("Zend\Form\Element\Number")', '');
        $annotationTags[] = new GenericTag('Annotation\Required(' . ($column->isNullable(
        ) ? 'false' : 'true') . ')', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StripTags"})', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StringTrim"})', '');
        $annotationTags[] = new GenericTag('Annotation\Attributes({"step":"0.01"})', '');

        if (!$column->isNullable()) {
            $annotationTags[] = new GenericTag('Annotation\Validator({"name":"StringLength", "options":{"min":"1"}})', '');
        }
        $label = ucfirst($columnName);
        $label = str_replace('_', ' ', $label);
        $annotationTags[] = new GenericTag('Annotation\Options({"label":"' . $label . '"})', '');

        $docblock = new DocBlockGenerator(
            null,
            null,
            array_merge($defaultTags, $annotationTags)
        );
        $docblock->setWordWrap(false);

        $params = array(
            'name' => $columnName,
            'defaultValue' => null,
            'flags' => PropertyGenerator::FLAG_PROTECTED,
        );

        if ($docblock) {
            $params['docblock'] = $docblock;
        }
        return PropertyGenerator::fromArray($params);
    }

    /**
     * Generate property and docblock for tiny integer fields.
     *
     * Also generates annotations for the form. Does not generate annotations if
     * the field name is in the skipAnnotationFields array. This so these fields will not show up in a add/edit form.
     *
     * Further foreign keys will be skipped since these will show up in other fields! Foreign keys will be determined
     * by ending on "_id".
     *
     * @param \Zend\Db\Metadata\Object\ColumnObject
     *
     * @return \Zend\Code\Generator\PropertyGenerator
     */
    protected function generateTinyIntProperty(\Zend\Db\Metadata\Object\ColumnObject $column)
    {

        $toolkitText = new \Trilectica\Toolkit\Text();
        $columnName = $column->getName();

        $skipAnnotationFields = array('id');
        $docblock = null;

        if (!in_array($column->getName(), $skipAnnotationFields) && substr($column->getName(), -3) !== '_id') {

            $annotationTags = array();

            $defaultTags = array(
                new GenericTag('description', 'Property ' . $columnName),
                new GenericTag('type', 'int'),
            );

            $annotationTags[] = new GenericTag('Annotation\Type("Zend\Form\Element\Checkbox")', '');
            $annotationTags[] = new GenericTag('Annotation\Required(' . ($column->isNullable(
            ) ? 'false' : 'true') . ')', '');
            $label = ucfirst($columnName);
            $label = str_replace('_', ' ', $label);
            $annotationTags[] = new GenericTag('Annotation\Options({"label":"' . $label . '"})', '');

            $docblock = new DocBlockGenerator(
                null,
                null,
                array_merge($defaultTags, $annotationTags)
            );
            $docblock->setWordWrap(false);
        }

        $params = array(
            'name' => $columnName,
            'defaultValue' => null,
            'flags' => PropertyGenerator::FLAG_PROTECTED,
        );

        if ($docblock) {
            $params['docblock'] = $docblock;
        }
        return PropertyGenerator::fromArray($params);
    }

    /**
     * Generate property and docblock for varchar fields.
     *
     * Also generates annotations for the form.
     *
     * @param \Zend\Db\Metadata\Object\ColumnObject
     *
     * @return \Zend\Code\Generator\PropertyGenerator
     */
    protected function generateVarcharProperty(\Zend\Db\Metadata\Object\ColumnObject $column)
    {

        $toolkitText = new \Trilectica\Toolkit\Text();
        $columnName = $column->getName();

        $defaultTags = array(
            new GenericTag('description', 'Property ' . $columnName),
            new GenericTag('type', 'string'),
        );

        if (strpos($columnName, 'email') !== false) {
            $annotationTags[] = new GenericTag('Annotation\Type("Zend\Form\Element\Email")', '');
        } else {
            $annotationTags[] = new GenericTag('Annotation\Type("Zend\Form\Element\Text")', '');
        }
        $annotationTags[] = new GenericTag('Annotation\Required(' . ($column->isNullable(
        ) ? 'false' : 'true') . ')', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StripTags"})', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StringTrim"})', '');
        if (!$column->isNullable()) {
            $annotationTags[] = new GenericTag('Annotation\Validator({"name":"StringLength", "options":{"min":"1", "max":"' . $column->getCharacterMaximumLength(
            ) . '"}})', '');
        } else {
            $annotationTags[] = new GenericTag('Annotation\Validator({"name":"StringLength", "options":{"min":"0", "max":"' . $column->getCharacterMaximumLength(
            ) . '"}})', '');
        }
        $label = ucfirst($columnName);
        $label = str_replace('_', ' ', $label);
        $annotationTags[] = new GenericTag('Annotation\Options({"label":"' . $label . '"})', '');

        $docblock = new DocBlockGenerator(
            null,
            null,
            array_merge($defaultTags, $annotationTags)
        );
        $docblock->setWordWrap(false);

        return PropertyGenerator::fromArray(
            array(
                'name' => $columnName,
                'defaultValue' => null,
                'flags' => PropertyGenerator::FLAG_PROTECTED,
                'docblock' => $docblock
            )
        );
    }

    /**
     * Generate property and docblock for password fields.
     *
     * Also generates annotations for the form.
     *
     * @param \Zend\Db\Metadata\Object\ColumnObject
     *
     * @return \Zend\Code\Generator\PropertyGenerator
     */
    protected function generatePasswordProperty(\Zend\Db\Metadata\Object\ColumnObject $column)
    {

        $toolkitText = new \Trilectica\Toolkit\Text();
        $columnName = $column->getName();

        $defaultTags = array(
            new GenericTag('description', 'Property ' . $columnName),
            new GenericTag('type', 'string'),
        );

        $annotationTags[] = new GenericTag('Annotation\Type("Zend\Form\Element\Password")', '');
        $annotationTags[] = new GenericTag('Annotation\Required(' . ($column->isNullable(
        ) ? 'false' : 'true') . ')', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StripTags"})', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StringTrim"})', '');
        if (!$column->isNullable()) {
            $annotationTags[] = new GenericTag('Annotation\Validator({"name":"StringLength", "options":{"min":"1", "max":"' . $column->getCharacterMaximumLength(
            ) . '"}})', '');
        } else {
            $annotationTags[] = new GenericTag('Annotation\Validator({"name":"StringLength", "options":{"min":"0", "max":"' . $column->getCharacterMaximumLength(
            ) . '"}})', '');
        }

        $label = ucfirst($columnName);
        $label = str_replace('_', ' ', $label);
        $annotationTags[] = new GenericTag('Annotation\Options({"label":"' . $label . '"})', '');

        $docblock = new DocBlockGenerator(
            null,
            null,
            array_merge($defaultTags, $annotationTags)
        );
        $docblock->setWordWrap(false);

        return PropertyGenerator::fromArray(
            array(
                'name' => $columnName,
                'defaultValue' => null,
                'flags' => PropertyGenerator::FLAG_PROTECTED,
                'docblock' => $docblock
            )
        );
    }

    /**
     * Generate property and docblock for text fields.
     *
     * Also generates annotations for the form.
     *
     * @param \Zend\Db\Metadata\Object\ColumnObject
     *
     * @return \Zend\Code\Generator\PropertyGenerator
     */
    protected function generateTextProperty(\Zend\Db\Metadata\Object\ColumnObject $column)
    {

        $toolkitText = new \Trilectica\Toolkit\Text();
        $columnName = $column->getName();

        $defaultTags = array(
            new GenericTag('description', 'Property ' . $columnName),
            new GenericTag('type', 'string'),
        );

        $annotationTags[] = new GenericTag('Annotation\Type("Zend\Form\Element\Textarea")', '');
        $annotationTags[] = new GenericTag('Annotation\Required(false)', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StringTrim"})', '');
        $label = ucfirst($columnName);
        $label = str_replace('_', ' ', $label);
        $annotationTags[] = new GenericTag('Annotation\Options({"label":"' . $label . '"})', '');

        $docblock = new DocBlockGenerator(
            null,
            null,
            array_merge($defaultTags, $annotationTags)
        );
        $docblock->setWordWrap(false);

        return PropertyGenerator::fromArray(
            array(
                'name' => $columnName,
                'defaultValue' => null,
                'flags' => PropertyGenerator::FLAG_PROTECTED,
                'docblock' => $docblock
            )
        );
    }

    /**
     * Generate property and docblock for timestamp fields.
     *
     * Also generates annotations for the form.
     *
     * @param \Zend\Db\Metadata\Object\ColumnObject
     *
     * @return \Zend\Code\Generator\PropertyGenerator
     */
    protected function generateTimestampProperty(\Zend\Db\Metadata\Object\ColumnObject $column)
    {

        $columnName = $column->getName();
        $skipAnnotationFields = array('created_on', 'updated_on');
        $defaultTags = array(
            new GenericTag('description', 'Property ' . $columnName),
            new GenericTag('type', 'string'),
        );

        $annotationTags[] = new GenericTag('Annotation\Type("Zend\Form\Element\DateTime")', '');
        $annotationTags[] = new GenericTag('Annotation\Required(' . ($column->isNullable(
        ) ? 'false' : 'true') . ')', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StripTags"})', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StringTrim"})', '');
        $annotationTags[] = new GenericTag('Annotation\Options({"name":"StringTrim"})', '');
        $annotationTags[] = new GenericTag('Annotation\Attributes({"step":"any"})', '');
        $label = ucfirst($columnName);
        $label = str_replace('_', ' ', $label);
        $annotationTags[] = new GenericTag('Annotation\Options({"label":"' . $label . '", "format":"Y-m-d H:i:s"})', '');

        $propertyParams = array(
            'name' => $columnName,
            'defaultValue' => null,
            'flags' => PropertyGenerator::FLAG_PROTECTED,
        );


        if (!in_array($columnName, $skipAnnotationFields)) {
            $docblock = new DocBlockGenerator(
                null,
                null,
                array_merge($defaultTags, $annotationTags)
            );
            $docblock->setWordWrap(false);
            $propertyParams['docblock'] = $docblock;
        }

        return PropertyGenerator::fromArray($propertyParams);
    }

    /**
     * Generate property and docblock for timestamp fields.
     *
     * Also generates annotations for the form.
     *
     * @param \Zend\Db\Metadata\Object\ColumnObject
     *
     * @return \Zend\Code\Generator\PropertyGenerator
     */
    protected function generateDateProperty(\Zend\Db\Metadata\Object\ColumnObject $column)
    {

        $toolkitText = new \Trilectica\Toolkit\Text();
        $columnName = $column->getName();

        $defaultTags = array(
            new GenericTag('description', 'Property ' . $columnName),
            new GenericTag('type', 'string'),
        );

        $annotationTags[] = new GenericTag('Annotation\Type("Zend\Form\Element\Date")', '');
        $annotationTags[] = new GenericTag('Annotation\Required(' . ($column->isNullable(
        ) ? 'false' : 'true') . ')', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StripTags"})', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StringTrim"})', '');
        if (!$column->isNullable()) {
            // this date format is important for validating date times.
            $annotationTags[] = new GenericTag('Annotation\Validator({"name":"Date","options":{"format":"Y-m-d"}})', '');
        }
        $label = ucfirst($columnName);
        $label = str_replace('_', ' ', $label);
        $annotationTags[] = new GenericTag('Annotation\Options({"label":"' . $label . '"})', '');

        $docblock = new DocBlockGenerator(
            null,
            null,
            array_merge($defaultTags, $annotationTags)
        );
        $docblock->setWordWrap(false);

        return PropertyGenerator::fromArray(
            array(
                'name' => $columnName,
                'defaultValue' => null,
                'flags' => PropertyGenerator::FLAG_PROTECTED,
                'docblock' => $docblock
            )
        );
    }


    /**
     * Generate property and docblock for enum fields.
     *
     * Also generates annotations for the form.
     *
     * @param \Zend\Db\Metadata\Object\ColumnObject
     *
     * @return \Zend\Code\Generator\PropertyGenerator
     */
    protected function generateEnumProperty(\Zend\Db\Metadata\Object\ColumnObject $column)
    {

        $toolkitText = new \Trilectica\Toolkit\Text();
        $columnName = $column->getName();

        $defaultTags = array(
            new GenericTag('description', 'Property ' . $columnName),
            new GenericTag('type', 'string'),
        );

        $enumValues = $column->getErrata('permitted_values');
        $valuesString = '';
        $validationString = '';
        foreach ($enumValues as $val) {
            if ($valuesString) {
                $valuesString .= ',';
                $validationString .= ',';
            }
            $valuesString .= '"' . $val . '":"' . $val . '"';
            $validationString .= '"' . $val . '"';
        }

        if ($column->isNullable()) {
            $valuesString = '"":"selecteer",' . $valuesString;
            $validationString = '"",' . $validationString;
        }

        $annotationTags[] = new GenericTag('Annotation\Type("Zend\Form\Element\Select")', '');
        $annotationTags[] = new GenericTag('Annotation\Required(' . ($column->isNullable(
        ) ? 'false' : 'true') . ')', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StripTags"})', '');
        $annotationTags[] = new GenericTag('Annotation\Filter({"name":"StringTrim"})', '');
        $label = ucfirst($columnName);
        $label = str_replace('_', ' ', $label);
        $annotationTags[] = new GenericTag('Annotation\Options({"label":"' . $label . '","value_options":{' . $valuesString . '}})', '');
        $annotationTags[] = new GenericTag('Annotation\Validator({"name":"InArray","options":{"haystack":{' . $validationString . '},"messages":{"notInArray":"Invalid type submitted"}}})', '');

        $docblock = new DocBlockGenerator(null, null, array_merge($defaultTags, $annotationTags));
        $docblock->setWordWrap(false);

        return PropertyGenerator::fromArray(
            array(
                'name' => $columnName,
                'defaultValue' => null,
                'flags' => PropertyGenerator::FLAG_PROTECTED,
                'docblock' => $docblock,
            )
        );
    }
}
