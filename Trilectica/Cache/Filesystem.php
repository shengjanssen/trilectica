<?php

namespace Trilectica\Cache;

use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\Http\PhpEnvironment\Response;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;
use ZF\Hal\View\HalJsonModel;

class Filesystem extends AbstractListenerAggregate
{
    /**
     * @var array
     */
    protected $cacheConfig  = array();

    /**
     * @var array
     */
    protected $config       = array();

    public function __construct($serviceManager, $config)
    {
        $this->sm       = $serviceManager;
        $this->config   = $config;
    }

    /**
     *
     * @param EventManagerInterface $events
     * @param int $priority
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_FINISH, array(
            $this,
            'onResponse'
        ), - 1000 );

        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH, array(
            $this,
            'onDispatch'
        ), 1000 );
    }

    public function onDispatch (MvcEvent $e)
    {
        /* @var $request HttpRequest */
        $request = $e->getRequest( );
        if ($request->getMethod( ) != "GET") {
            return;
        }

        $matches = $e->getRouteMatch();
        if (! $matches) {
            return;
        }

        $route = $matches->getMatchedRouteName();
        $routes  = $this->config['routes'];

        if (!in_array($route, $routes)) {
            return;
        }

        // only cache api requests.
        if (strpos($route, '.rest.') === false && strpos($route, '.rpc.') === false) {
            return;
        }

        // option to disable cache
        if ($request->getQuery('cache', "on") != "on") {
            return;
        }

        $cacheService = new \Zend\Cache\Storage\Adapter\Filesystem($this->config['options']);
        $identity     = $this->sm->get('api-identity');
        if ($identity) {
            $identity = $identity->getAuthenticationIdentity();
            $token = $identity['access_token'];
        } else {
            $token = '';
        }

        $hash = hash('sha256', $route . $token);
        $res = $cacheService->hasItem($hash) ;

        if ($res !== false) {
            $output = $cacheService->getItem($hash);

            $response = new JsonModel(json_decode($output, true));
            /** @var Response $responsex */
            $e->getResponse()->setStatusCode(200);
            $e->stopPropagation(true);
            $e->setViewModel($response);
            $e->setResult($response);

            return $response;
        }
    }

    public function onResponse(MvcEvent $e)
    {
        /* @var $request \Zend\Http\Request */
        $request = $e->getRequest( );

        /* @var $response \Zend\Http\Response */
        $response = $e->getResponse( );

        if (! $response instanceof \Zend\Http\Response) {
            return;
        }

        $matches = $e->getRouteMatch();
        if (! $matches) {
            return;
        }

        $route  = $matches->getMatchedRouteName();
        $routes = $this->config['routes'];

        if (!in_array($route, $routes)) {
            return;
        }

        if (strpos($route, '.rest.') === false && strpos($route, '.rpc.') === false) {
            return;
        }

        if ($request->getQuery('cache', "on") != "on") {
            return;
        }

        $cacheService = new \Zend\Cache\Storage\Adapter\Filesystem($this->config['options']);
        $identity     = $this->sm->get('api-identity');
        if ($identity) {
            $identity = $identity->getAuthenticationIdentity();
            $token = $identity['access_token'];
        } else {
            $token = '';
        }

        $hash = hash('sha256', $route . $token);

        /* @var $output HalJsonModel */
        $output = $e->getViewModel();
        if ($output instanceof HalJsonModel) {
            $cacheService->setItem($hash, $response->getBody());
        }
    }
}