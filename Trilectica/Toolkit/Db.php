<?php

namespace Trilectica\Toolkit;

class Db
{
    public function tableToObjectName($tableName)
    {
        $toolkitText = $this->getTextToolkit();
        return ucfirst($toolkitText->underscoreToUpper($tableName));
    }

    protected function getTextToolkit()
    {
        return new \Trilectica\Toolkit\Text();
    }
}