<?php

namespace Trilectica\Toolkit;

/**
 * Helpers for Strings.
 *
 * @author Trilectica Internet Solutions
 */
class Text
{

    /**
     * Replaces the underscore and the following letter by the following letter capitalized.
     *
     * @param $name
     * @return string
     */
    public function underscoreToUpper($name)
    {
        $name = strtolower($name);
        $tmp = explode('_', $name);
        $newName = '';
        foreach ($tmp as $index => $part) {
            if ($index == 0) {
                $newName .= $part;
            } else {
                $newName .= ucfirst($part);
            }
        }
        return $newName;
    }

    /**
     * Return the plural form for a string.
     *
     * @param $name
     * @return string
     */
    public function plural($name)
    {
        if (substr($name, -1) == 's') {
            return $name . 'es';
        } elseif (substr($name, -1) == 'y') {
            return substr($name, 0, strlen($name) - 1) . 'ies';
        } else {
            return $name . 's';
        }
    }

    public function random($length = 8)
    {
        return substr(
            str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()-_+=,."),
            0,
            $length
        );
    }

    public function stripTitle($txt)
    {
        // return if empty
        if (empty($txt)) {
            return false;
        }

        // lowercase
        $txt = strtolower($txt);

        // replace spaces by "-"
        // convert accents to html entities
        $txt = htmlentities(str_replace(' ', '-', $txt));

        // remove the accent from the letter
        $txt = preg_replace(
            array(
                '@&([a-zA-Z]){1,2}(acute|grave|circ|tilde|uml|ring|elig|zlig|slash|cedil|strok|lig){1};@',
                '@&[euro]{1};@'
            ),
            array(
                '${1}',
                'E'
            ),
            $txt
        );

        // now, everything but alphanumeric and -_ can be removed
        // also remove double dashes
        $txt = preg_replace(array('@[^a-zA-Z0-9\-_]@', '@[\-]{2,}@'), array('', '-'), html_entity_decode($txt));
        $txt = trim($txt, '-');

        return $txt;
    }
}