<?php

namespace Trilectica\Language;

use \Zend\ServiceManager\ServiceLocatorInterface;

class Factory implements \Zend\ServiceManager\FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $sm = $serviceLocator->getServiceLocator();
        return new Loader($sm);
    }
}