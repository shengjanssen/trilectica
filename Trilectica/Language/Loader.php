<?php

namespace Trilectica\Language;

use \Zend\I18n\Translator\Loader\RemoteLoaderInterface;
use \Zend\I18n\Translator\TextDomain;
use \Zend\ServiceManager\ServiceLocatorAwareInterface;

class Loader implements RemoteLoaderInterface, ServiceLocatorAwareInterface
{

    protected $sl;

    public function load($locale, $textDomain)
    {
        $messages = array();

        $serviceManager = $this->getServiceLocator()->getServiceLocator();

        $localeId = $serviceManager->get('LocaleManager')->getId($locale);
        if ($localeId) {
            /** @var $manager \Model\Managers\TranslationManager */
            $manager = $serviceManager->get('TranslationManager');
            $translations = $manager->fetchByLocaleId($localeId);
            foreach ($translations as $translation) {
                $messages[$translation->getKey()] = $translation->getTranslation();
            }
        }

        if (!is_array($messages)) {
            throw new \Zend\I18N\Exception\InvalidArgumentException(sprintf(
                'Expected an array, but received %s',
                gettype($messages)
            ));
        }

        $textDomain = new TextDomain($messages);

        if (array_key_exists('', $textDomain)) {
            if (isset($textDomain['']['plural_forms'])) {
                $textDomain->setPluralRule(
                    PluralRule::fromString($textDomain['']['plural_forms'])
                );
            }

            unset($textDomain['']);
        }

        return $textDomain;
    }

    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $sl)
    {
        $this->sl = $sl;
    }

    public function getServiceLocator()
    {
        return $this->sl;
    }

}
