<?php

namespace Trilectica\Auth;

use \Zend\Mvc\MvcEvent;
use \Zend\Authentication\Storage;
use \Zend\Authentication\AuthenticationService;
use \Zend\ServiceManager\ServiceManager;
use \Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter;
use \Zend\Authentication\Storage\Session;
use \Trilectica\ServiceLocatorAwareInterface\Object as ServiceLocatorObject;

/**
 * This service is to authenticate a users credentials through
 * the user table. If not authorized he will be redirected to the admin-login
 * route.
 *
 * The authentication uses the salt from the config section if available. This
 * salt should then of course also be used for the Registration section of the used system.
 *
 * @owner Trilectica Internet Solutions
 * @author Stefan van de Kaa
 */
class Service extends ServiceLocatorObject
{

    /**
     * Check if the user is authenticated to view the backend.
     *
     * Redirects to the admin-login route if the user is not authenticated.
     * Uses the service managers AuthService.
     *
     * @param \Zend\Mvc\MvcEvent $e
     */
    public function checkAuth(MvcEvent $e)
    {
        $match = $e->getRouteMatch();

        $name = $match->getMatchedRouteName();
        $list = array('admin-login', 'admin-login-hash', 'admin-logout', 'front-login', 'front-logout');
        if (in_array($name, $list)) {
            return;
        }

        $auth = $this->getAuthAdapter();
        if (!$auth->hasIdentity()) {
            $response = $e->getResponse();

            //location to page or what ever
            $app = explode('-', $name);
            $route = $e->getRouter()->assemble(array(), array('name' => $app[0] . '-login'));
            $response->getHeaders()->addHeaderLine('Location', $route);
            $response->setStatusCode(302);
            $response->sendHeaders();
            exit;
        }
    }

    /**
     * Fetch the auth adapter used for the authentication service
     *
     * @return \Zend\Authentication\AuthenticationService
     */
    public function getAuthAdapter()
    {
        $dbAdapter = $this->sm->get('default_db');
        $config = $this->sm->get('config');
        if (isset($config["salt"])) {
            $dbTableAuthAdapter = new CredentialTreatmentAdapter($dbAdapter, 'user', 'username', 'password', 'MD5(CONCAT(?, "' . $config['salt'] . '"))');
        } else {
            $dbTableAuthAdapter = new CredentialTreatmentAdapter($dbAdapter, 'user', 'username', 'password', 'MD5(?)');
        }

        $authService = new AuthenticationService();
        $authService->setAdapter($dbTableAuthAdapter);
        $authService->setStorage(new CookieStorage());
        return $authService;
    }

    /**
     * Get the authenticated user.
     *
     * @return User | bool
     */
    public function getAuthenticatedUser()
    {
        $auth = $this->getAuthAdapter();

        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();

            $userManager = $this->getServiceLocator()->get('UserManager');
            try {
                return $userManager->findByUsername($identity);
            } catch (\Exception $ex) {
                return false;
            }
        }
    }
}
