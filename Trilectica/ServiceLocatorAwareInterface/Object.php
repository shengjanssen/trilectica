<?php

namespace Trilectica\ServiceLocatorAwareInterface;

use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorAwareInterface;
use \Zend\ServiceManager\ServiceLocatorInterface;

/**
 * This service is used to validate if the authenticated user has access
 * to the route he is trying to access.
 *
 * The authenticated user will be fetched from the AuthService.
 *
 * @owner Trilectica Internet Solutions
 * @author Stefan van de Kaa
 */
class Object implements ServiceLocatorAwareInterface
{

    /** @var ServiceLocatorInterface */
    protected $sm;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->sm = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->sm;
    }
}
