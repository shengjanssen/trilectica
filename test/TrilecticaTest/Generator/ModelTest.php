<?php
namespace TrilecticaTest\Generator;

use PHPUnit_Framework_TestCase;

use Trilectica\Generator\Model;

class ModelTest extends PHPUnit_Framework_TestCase
{
    protected $testAdapter = null;

    public function setUp() {
        $mockFunctions = array();

        $this->testAdapter = $this->getMock(
            '\Zend\Db\Adapter\Adapter',
            $mockFunctions,
            array(
                array(
                    'driver' => 'Pdo_Mysql',
                    'database' => 'test',
                    'username' => 'test',
                    'password' => 'test',
                )
            )
        );
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testConstructWithoutParams() {
        new Model();
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testConstructWithInvalidParam() {
        new Model(new \stdClass(), 'test');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testConstructWithInvalidParam2() {


        new Model($this->testAdapter);
    }

    public function testGetDbAndSetDb() {
        $model = new Model($this->testAdapter, 'test');
        $this->assertEquals($this->testAdapter, $model->getDb());
    }

    public function testGetDbNameAndSetDbName() {
        $model = new Model($this->testAdapter, 'test');
        $this->assertEquals('test', $model->getDbName());
    }
/*
    public function testBuildCallsCreateNecessaryDirectories() {

        $mock = $this->getMock(
            'Trilectica\Generator\Model',
            array('getTableNames', 'getTableInfo'),
            array($this->testAdapter, 'test')
        );

        $mock->expects($this->once())
            ->method('getTableNames')
            ->will($this->returnValue(array('test'))
        );

        $tableColumnMock = new \Zend\Db\Metadata\Object\ColumnObject('kolom1', 'test');
        $tableColumnMock2 = new \Zend\Db\Metadata\Object\ColumnObject('kolom2', 'test');

        $tableMock = $this->getMock('\Zend\Db\Metadata\Object\TableObject', array('getColumns'), array('test'));
        $tableMock->expects($this->once())
            ->method('getColumns')
            ->will($this->returnValue(array($tableColumnMock, $tableColumnMock2)));

        $constraintsMock = $this->getMock(
            '\Zend\Db\Metadata\Object\ConstraintObject',
            array('hasColumns', 'isForeignKey', 'getColumns', 'getReferencedColumns', 'getReferencedTableName'),
            array('test', 'test')
        );
        $constraintsMock->expects($this->exactly(3))
            ->method('hasColumns')
            ->will($this->returnValue(true));

        $constraintsMock->expects($this->exactly(3))
            ->method('isForeignKey')
            ->will($this->returnValue(true));

        $constraintsMock->expects($this->exactly(7))
            ->method('getColumns')
            ->will($this->returnValue(array('kolom1')));


        $constraintsMock->expects($this->exactly(1))
            ->method('getReferencedTableName')
            ->will($this->returnValue('test2'));

        $tableInfoMock = $this->getMock(
            '\Zend\Db\Metadata\Metadata',
            array('getTable', 'getConstraints', 'getTableNames'),
            array($this->testAdapter),
            '',
            false
        );
        $tableInfoMock->expects($this->once())
            ->method('getTable')
            ->will($this->returnValue($tableMock));

        $tableInfoMock->expects($this->once())
            ->method('getTableNames')
            ->will($this->returnValue(array('test', 'test2')));

        $tableInfoMock->expects($this->exactly(3))
            ->method('getConstraints')
            ->will($this->returnValue(array($constraintsMock)));

        $mock->expects($this->once())
            ->method('getTableInfo')
            ->will($this->returnValue($tableInfoMock)
        );

        $mock->build();
    }
*/
}