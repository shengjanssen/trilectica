<?php

namespace TrilecticaTest\Toolkit;

use PHPUnit_Framework_TestCase;

use Trilectica\Toolkit\Db;

class DbTest extends PHPUnit_Framework_TestCase {
    public function testTableToObjectName() {
        $toolkit = new Db;
        $this->assertEquals('TestTest', $toolkit->tableToObjectName('test_test'));
    }
}