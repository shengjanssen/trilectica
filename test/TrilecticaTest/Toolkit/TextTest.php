<?php

namespace TrilecticaTest\Toolkit;

use PHPUnit_Framework_TestCase;

use Trilectica\Toolkit\Text;

class TextTest extends PHPUnit_Framework_TestCase {
    public function testUnderscoreToUpper() {
        $toolkit = new Text;
        $this->assertEquals('testTest', $toolkit->underscoreToUpper('test_test'));
        $this->assertEquals('testTEst', $toolkit->underscoreToUpper('test_t__est'));
        $this->assertEquals('', $toolkit->underscoreToUpper('_'));
    }

    public function testPlural() {
        $toolkit = new Text;
        $this->assertEquals('testses',$toolkit->plural('tests'));
        $this->assertEquals('tasties',$toolkit->plural('tasty'));
        $this->assertEquals('tests',$toolkit->plural('test'));
    }
}